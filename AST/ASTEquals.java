package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTEquals implements ASTNode {

	ASTNode left, right;

	public ASTEquals(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		if (left.eval(env) == right.eval(env)) {
			return 1;
		}
		return 0;
	}
	
	@Override
	public String toString() {
		return left.toString() + " == " + right.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_div();
		
	}
}
