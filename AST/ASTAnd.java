package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTAnd implements ASTNode {

	ASTNode left, right;
	
	public ASTAnd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		if (left.eval(env) > 0 && right.eval(env) > 0) {
			return 1;
		}
		return 0;
	
	}
	
	@Override
	public String toString() {
		return left.toString() + " && " + right.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_and();
		
	}
}
